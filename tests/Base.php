<?php

namespace Treerful\InvoiceTest;

use Orchestra\Testbench\TestCase;

class Base extends TestCase
{
    protected function getPackageProviders($app)
    {
        return ['Treerful\Invoice\Pay2goInvoiceServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('pay2goinv.TaxRate', '5');
    }
}
