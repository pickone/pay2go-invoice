<?php

namespace Treerful\InvoiceTest;

use Treerful\InvoiceTest\Base as TestCase;
use Treerful\Invoice\Pay2goInvoice\AllowInvoice;

class AllowAmountTest extends TestCase
{
    public function getProperty($object, $property)
    {
        $reflectedClass = new \ReflectionClass ($object);
        $reflection = $reflectedClass->getProperty($property);
        $reflection->setAccessible(true);
        return $reflection->getValue($object);
    }

    public function testOneQuantityForB2B()
    {
        // $this->markTestSkipped();
        // B2B 數量 1
        // 金額 6667

        // 數量 1
        // 折讓商品單價(ItemPrice) = 未稅價 = round(6667 / 1.05) = 6350
        // 折讓商品小記(ItemAmt) = 6350 * 1 = 6350
        // 折讓商品稅額(ItemTaxAmt) = 6667 - 6350 = 317
        // 折讓總金額(TotalAmt) = 6667

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'ItemPrice' => 6667,
            'BuyerEmail' => '',
            'ItemCount' => 1,
            'ItemUnit' => '個',
            'BuyerType' => 2,
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals(6350, $postData['ItemPrice']);
        $this->assertEquals(6350, $postData['ItemAmt']);
        $this->assertEquals(317, $postData['ItemTaxAmt']);
        $this->assertEquals(6667, $postData['TotalAmt']);
    }

    public function testOneQuantityForB2C(): void
    {
        // $this->markTestSkipped();
        // B2C 數量 1
        // 金額 6667

        // 數量 1
        // 折讓商品單價(ItemPrice) = 含稅價 = 6667
        // 折讓商品稅額(ItemTaxAmt) = round(6667 * 1 / 1.05 * 0.05) = 317
        // 折讓總金額(TotalAmt) = 6667
        // 折讓商品小記(ItemAmt) = 6667 - 317 = 6350

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'ItemPrice' => 6667,
            'BuyerEmail' => '',
            'ItemCount' => 1,
            'ItemUnit' => '個',
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals(6667, $postData['ItemPrice']);
        $this->assertEquals(6350, $postData['ItemAmt']);
        $this->assertEquals(317, $postData['ItemTaxAmt']);
        $this->assertEquals(6667, $postData['TotalAmt']);
    }

    public function testMulitQuantitiesForB2B(): void
    {
        // $this->markTestSkipped();
        // B2B 數量 3
        // 金額 1960

        // 折讓商品單價(ItemPrice) = 未稅價 = round(5880 / 3 / 1.05) = 1867
        // 折讓商品小記(ItemAmt) = round(5880 / 3 / 1.05 * 3) = 5600
        // 折讓商品稅額(ItemTaxAmt) = 5880 - 5600 = 280
        // 折讓總金額(TotalAmt) = 5880

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'ItemPrice' => 1960,
            'BuyerEmail' => '',
            'ItemCount' => 3,
            'ItemUnit' => '個',
            'BuyerType' => 2,
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals(1867, $postData['ItemPrice']);
        $this->assertEquals(5600, $postData['ItemAmt']);
        $this->assertEquals(280, $postData['ItemTaxAmt']);
        $this->assertEquals(5880, $postData['TotalAmt']);
    }

    public function testMulitQuantitiesForB2C(): void
    {
        // $this->markTestSkipped();
        // B2C 數量 3
        // 金額 1960

        // 折讓商品單價(ItemPrice) = 1960
        // 折讓總金額(TotalAmt) = 1960 * 3 = 5880
        // 折讓商品稅額(ItemTaxAmt) = 1960 * 3 / 1.05 * 0.05 = 280
        // 折讓商品小記(ItemAmt) = 1960 * 3 - 280 = 5600

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'ItemPrice' => 1960,
            'BuyerEmail' => '',
            'ItemCount' => 3,
            'ItemUnit' => '個',
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals(1960, $postData['ItemPrice']);
        $this->assertEquals(5600, $postData['ItemAmt']);
        $this->assertEquals(280, $postData['ItemTaxAmt']);
        $this->assertEquals(5880, $postData['TotalAmt']);
    }

    public function testMulitItemForB2B(): void
    {
        // $this->markTestSkipped();
        // B2B 兩項
        // 商品A $1960 數量 3
        // 商品B $1250 數量 2

        // 商品單價(ItemPrice)
        // 商品A round(1960 /1.05) = 1867
        // 商品B round(1250 / 1.05) = 1190

        // 商品小記(ItemAmt)
        // 商品A round(1960 /1.05 * 3) = 5600
        // 商品B round(1250 / 1.05 * 2) = 2380

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'BuyerEmail' => '',
            'ItemName' => ['商品A', '商品B'],
            'ItemPrice' => [1960, 1250],
            'ItemCount' => [3, 2],
            'ItemUnit' => ['個', '個'],
            'BuyerType' => 2,
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals('1867|1190', $postData['ItemPrice']);
        $this->assertEquals('5600|2381', $postData['ItemAmt']);
        $this->assertEquals('280|119', $postData['ItemTaxAmt']);
        $this->assertEquals(8380, $postData['TotalAmt']);
    }

    public function testMulitItemForB2C(): void
    {
        // $this->markTestSkipped();
        // B2C 兩項
        // 商品A $1960 數量 3
        // 商品B $1250 數量 2

        // 商品單價(ItemPrice) = 個別給予含稅價
        // 商品A 1960
        // 商品B 1250

        // 商品小記(ItemAmt) = 個別給予含稅價
        // 商品A 1960 * 3 = 5880
        // 商品B 1250 * 2 = 2000

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'BuyerEmail' => '',
            'ItemName' => ['商品A', '商品B'],
            'ItemPrice' => [1960, 1250],
            'ItemCount' => [3, 2],
            'ItemUnit' => ['個', '個'],
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals('1960|1250', $postData['ItemPrice']);
        $this->assertEquals('5600|2381', $postData['ItemAmt']);
        $this->assertEquals('280|119', $postData['ItemTaxAmt']);
        $this->assertEquals(8380, $postData['TotalAmt']);
    }

    public function testOneItemMaxTaxLimit(): void
    {
        // $this->markTestSkipped();
        // B2B 數量 3
        // 金額 1960

        // 折讓商品單價(ItemPrice) = 未稅價 = round(5880 / 3 / 1.05) = 1867
        // 折讓商品小記(ItemAmt) = round(5880 / 3 / 1.05 * 3) = 5600
        // 折讓商品稅額(ItemTaxAmt) = 5880 - 5600 = 280
        // 折讓總金額(TotalAmt) = 5880

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'ItemPrice' => 1960,
            'BuyerEmail' => '',
            'ItemCount' => 3,
            'ItemUnit' => '個',
            'BuyerType' => 2,
            'MaxTax' => 250,
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals(1867, $postData['ItemPrice']);
        $this->assertEquals(5630, $postData['ItemAmt']);
        $this->assertEquals(250, $postData['ItemTaxAmt']);
        $this->assertEquals(5880, $postData['TotalAmt']);
    }

    public function testMulitItemMaxTaxLimit(): void
    {
        // $this->markTestSkipped();
        // B2C 兩項
        // 商品A $1960 數量 3
        // 商品B $1250 數量 2

        // 商品單價(ItemPrice) = 個別給予含稅價
        // 商品A 1960
        // 商品B 1250

        // 商品小記(ItemAmt) = 個別給予含稅價
        // 商品A 1960 * 3 = 5880
        // 商品B 1250 * 2 = 2000

        $AllowInvoice = new AllowInvoice();

        $invoiceData = [
            'BuyerEmail' => '',
            'ItemName' => ['商品A', '商品B'],
            'ItemPrice' => [1960, 1250],
            'ItemCount' => [3, 2],
            'ItemUnit' => ['個', '個'],
            'MaxTax' => 380,
        ];

        $AllowInvoice->setData($invoiceData);

        $postData = $this->getProperty($AllowInvoice, 'postData');

        $this->assertEquals('1960|1250', $postData['ItemPrice']);
        $this->assertEquals('5600|2400', $postData['ItemAmt']);
        $this->assertEquals('280|100', $postData['ItemTaxAmt']);
        $this->assertEquals(8380, $postData['TotalAmt']);
    }
}
