<?php

namespace Treerful\InvoiceTest;

use Treerful\InvoiceTest\Base as TestCase;
use Treerful\Invoice\Pay2goInvoice\CreateInvoice;

class CreateAmountTest extends TestCase
{
    public function getProperty($object, $property)
    {
        $reflectedClass = new \ReflectionClass($object);
        $reflection = $reflectedClass->getProperty($property);
        $reflection->setAccessible(true);
        return $reflection->getValue($object);
    }

    public function testOneQuantityForB2B()
    {
        // $this->markTestSkipped();
        // B2B 數量 1
        // 金額 6667

        // 數量 1
        // 商品單價(ItemPrice) = 未稅價 = round(6667 / 1.05) = 6350
        // 商品小記(ItemAmt) = 6350 * 1 = 6350
        // 銷售額(amt) = $6350
        // B2B 稅額(TaxAmt) = 6667 - 6350 = 317
        // 含稅總額(TotalAmt) = 6667

        $createInvoice = new CreateInvoice();

        $invoiceData = [
            'BuyerUBN' => '12345678',
            'BuyerType' => 2,
            'CarrierType' => '',
            'ItemName' => '商品1',
            'ItemPrice' => 6667,
            'ItemCount' => 1,
            'ItemUnit' => '個',
        ];

        $createInvoice->setData($invoiceData);

        $postData = $this->getProperty($createInvoice, 'postData');

        $this->assertEquals(6350, $postData['ItemPrice']);
        $this->assertEquals(6350, $postData['ItemAmt']);
        $this->assertEquals(6350, $postData['Amt']);
        $this->assertEquals(317, $postData['TaxAmt']);
        $this->assertEquals(6667, $postData['TotalAmt']);
    }

    public function testOneQuantityForB2C(): void
    {
        // $this->markTestSkipped();
        // B2C 數量 1
        // 金額 6667

        // 商品單價(ItemPrice) = 含稅價 = 6667
        // 商品小記(ItemAmt) = 6667 * 1 = 6667
        // 銷售額(amt) = round(6667 / 1.05) = 6350
        // B2C 稅額(TaxAmt) = 6667 - 6350 = 317
        // 含稅總額(TotalAmt) = 6667

        $createInvoice = new CreateInvoice();

        $invoiceData = [
            'BuyerType' => 0,
            'CarrierType' => 0,
            'CarrierNum' => '',
            'ItemName' => '商品1',
            'ItemPrice' => 6667,
            'ItemCount' => 1,
            'ItemUnit' => '個',
        ];

        $createInvoice->setData($invoiceData);

        $postData = $this->getProperty($createInvoice, 'postData');

        $this->assertEquals(6667, $postData['ItemPrice']);
        $this->assertEquals(6667, $postData['ItemAmt']);
        $this->assertEquals(6350, $postData['Amt']);
        $this->assertEquals(317, $postData['TaxAmt']);
        $this->assertEquals(6667, $postData['TotalAmt']);
    }

    public function testMulitQuantitiesForB2B(): void
    {
        // $this->markTestSkipped();
        // B2B 數量 3
        // 金額 1960

        // 數量 1
        // 商品單價(ItemPrice) = 未稅價 = round(1960 / 1.05) = 1867
        // 商品小記(ItemAmt) = round(1960 * 3 / 1.05) = 5600
        // 銷售額(amt) = $5600
        // B2B 稅額(TaxAmt) = 1960 * 3 - 5600 = 280
        // 含稅總額(TotalAmt) = 1960 * 3 = 5880

        $createInvoice = new CreateInvoice();

        $invoiceData = [
            'BuyerType' => 2,
            'BuyerUBN' => '12345678',
            'CarrierType' => '',
            'ItemName' => '商品1',
            'ItemPrice' => 1960,
            'ItemCount' => 3,
            'ItemUnit' => '個',
        ];

        $createInvoice->setData($invoiceData);

        $postData = $this->getProperty($createInvoice, 'postData');

        $this->assertEquals(1867, $postData['ItemPrice']);
        $this->assertEquals(5600, $postData['ItemAmt']);
        $this->assertEquals(5600, $postData['Amt']);
        $this->assertEquals(280, $postData['TaxAmt']);
        $this->assertEquals(5880, $postData['TotalAmt']);
    }

    public function testMulitQuantitiesForB2C(): void
    {
        // $this->markTestSkipped();
        // B2C 數量 3

        // 數量 3
        // B2C 稅額 = 1960 * 3 / 1.05 * 0.05 = 280
        // 含稅總額 = 1960 * 3 = 5880
        // 銷售額 = 1960 * 3 - 280 = 5600

        $createInvoice = new CreateInvoice();

        $invoiceData = [
            'BuyerType' => 0,
            'CarrierType' => 0,
            'CarrierNum' => '',
            'ItemName' => '商品1',
            'ItemPrice' => 1960,
            'ItemCount' => 3,
            'ItemUnit' => '個',
        ];

        $createInvoice->setData($invoiceData);

        $postData = $this->getProperty($createInvoice, 'postData');

        $this->assertEquals(1960, $postData['ItemPrice']);
        $this->assertEquals(5880, $postData['ItemAmt']);
        $this->assertEquals(5600, $postData['Amt']);
        $this->assertEquals(280, $postData['TaxAmt']);
        $this->assertEquals(5880, $postData['TotalAmt']);
    }

    public function testMulitItemForB2B(): void
    {
        // $this->markTestSkipped();
        // B2B 兩項
        // 商品A $880 數量 2
        // 商品B $500 數量 1

        // 商品單價(ItemPrice) = 個別給予含稅價
        // 商品A 880
        // 商品B 500

        // 商品小記(ItemAmt) = 個別給予含稅價
        // 商品A 880 * 2 = 1760
        // 商品B 500 * 1 = 500

        // 銷售額(amt) = (880 * 2 + 500 * 1) / 1.05 = 2152
        // B2B 稅額(TaxAmt) = 880 * 2 + 500 * 1 - 2151 = 108
        // 含稅總額(TotalAmt) = 880 * 2 + 500 * 1 = 2260

        $createInvoice = new CreateInvoice();

        $invoiceData = [
            'BuyerType' => 2,
            'BuyerUBN' => '12345678',
            'CarrierType' => '',
            'ItemName' => ['商品A', '商品B'],
            'ItemPrice' => [880, 500],
            'ItemCount' => [2, 1],
            'ItemUnit' => ['個', '個'],
        ];

        $createInvoice->setData($invoiceData);

        $postData = $this->getProperty($createInvoice, 'postData');

        $this->assertEquals('880|500', $postData['ItemPrice']);
        $this->assertEquals('1760|500', $postData['ItemAmt']);
        $this->assertEquals(2152, $postData['Amt']);
        $this->assertEquals(108, $postData['TaxAmt']);
        $this->assertEquals(2260, $postData['TotalAmt']);
    }

    public function testMulitItemForB2C(): void
    {
        // $this->markTestSkipped();
        // B2C 兩項
        // 商品A $880 數量 2
        // 商品B $500 數量 1

        // 商品單價(ItemPrice) = 個別給予含稅價
        // 商品A 880
        // 商品B 500

        // 商品小記(ItemAmt) = 個別給予含稅價
        // 商品A 880 * 2 = 1760
        // 商品B 500 * 1 = 500

        // 銷售額(amt) = (880 * 2 + 500 * 1) / 1.05 = 2152
        // B2C 稅額(TaxAmt) = 880 * 2 + 500 * 1 - 2151 = 108
        // 含稅總額(TotalAmt) = 880 * 2 + 500 * 1 = 2260

        $createInvoice = new CreateInvoice();

        $invoiceData = [
            'BuyerType' => 0,
            'CarrierType' => 0,
            'CarrierNum' => '',
            'ItemName' => ['商品A', '商品B'],
            'ItemPrice' => [880, 500],
            'ItemCount' => [2, 1],
            'ItemUnit' => ['個', '個'],
        ];

        $createInvoice->setData($invoiceData);

        $postData = $this->getProperty($createInvoice, 'postData');

        $this->assertEquals('880|500', $postData['ItemPrice']);
        $this->assertEquals('1760|500', $postData['ItemAmt']);
        $this->assertEquals(2152, $postData['Amt']);
        $this->assertEquals(108, $postData['TaxAmt']);
        $this->assertEquals(2260, $postData['TotalAmt']);
    }
}
