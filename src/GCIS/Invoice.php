<?php

namespace Treerful\Invoice\GCIS;

class Invoice
{
    public function getCompanyName($no)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://data.gcis.nat.gov.tw/od/data/api/9D17AE0D-09B5-4732-A8F4-81ADED04B679?$format=json&$filter=Business_Accounting_NO%20eq%20' . $no);
        curl_setopt($ch, CURLOPT_TIMEOUT, 7);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            $error = curl_errno($ch);
        }

        curl_close($ch);

        return [
            'http_status' => $httpStatus,
            'error' => $error ?? null,
            'response' => $response,
        ];
    }

    public function getBusinessName($no)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://data.gcis.nat.gov.tw/od/data/api/855A3C87-003A-4930-AA4B-2F4130D713DC?$format=json&$filter=President_No%20eq%20' . $no);
        curl_setopt($ch, CURLOPT_TIMEOUT, 7);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            $error = curl_errno($ch);
        }

        curl_close($ch);

        return [
            'http_status' => $httpStatus,
            'error' => $error ?? null,
            'response' => $response,
        ];
    }
}
