<?php

namespace Treerful\Invoice\Pay2goInvoice;

class AllowInvoice extends Pay2GoInvoice
{
    /*
     * 折讓發票的 class
     */
    protected $taxRate;

    public function setData($data): array
    {
        $this->setDataByFields($data);

        if (isset($data['BuyerType']) && $data['BuyerType'] == 2) {
            if (is_array($data['ItemPrice'])) {
                $this->multiItemB2B($data);
            } else {
                $this->oneItemB2B($data);
            }
        } else {
            if (is_array($data['ItemPrice'])) {
                $this->multiTiemB2C($data);
            } else {
                $this->oneItemB2C($data);
            }
        }

        $this->checkMaxTax($data);

        $this->checkMultiItem();

        return $this->postData;
    }

    protected function oneItemB2B($data)
    {
        $this->postData['ItemPrice'] = round($data['ItemPrice'] / $this->taxRate);

        $total = $data['ItemPrice'] * $data['ItemCount'];
        $this->postData['ItemAmt'] = round($total / $this->taxRate);
        $this->postData['ItemTaxAmt'] = $total - $this->postData['ItemAmt'];
        $this->postData['TotalAmt'] = $total;
    }

    protected function multiItemB2B($data)
    {
        $this->multiDefault();

        foreach ($data['ItemPrice'] as $key => $itemPrice) {
            $this->postData['ItemPrice'][$key] = round($itemPrice / $this->taxRate);

            $total = $itemPrice * $data['ItemCount'][$key];
            $this->postData['ItemAmt'][$key] = round($total / $this->taxRate);
            $this->postData['ItemTaxAmt'][$key] = $total - $this->postData['ItemAmt'][$key];
            $this->postData['TotalAmt'] += $total;
        }
    }

    protected function oneItemB2C($data)
    {
        $this->postData['ItemPrice'] = $data['ItemPrice'];

        $total = $data['ItemPrice'] * $data['ItemCount'];
        $this->postData['ItemTaxAmt'] = round($total / $this->taxRate * config('pay2goinv.TaxRate') * 0.01);
        $this->postData['TotalAmt'] = $total;
        $this->postData['ItemAmt'] = $total - $this->postData['ItemTaxAmt'];
    }

    protected function multiTiemB2C($data)
    {
        $this->multiDefault();

        foreach ($data['ItemPrice'] as $key => $itemPrice) {
            $this->postData['ItemPrice'][$key] = $itemPrice;

            $total = $itemPrice * $this->postData['ItemCount'][$key];
            $this->postData['ItemTaxAmt'][$key] = round($total / $this->taxRate * config('pay2goinv.TaxRate') * 0.01);
            $this->postData['TotalAmt'] += $total;
            $this->postData['ItemAmt'][$key] = $total - $this->postData['ItemTaxAmt'][$key];
        }
    }

    protected function setUrl()
    {
        if (!$this->debugMode) {
            $this->pay2goUrl = config('pay2goinv.Url_Allow');
        } else {
            $this->pay2goUrl = config('pay2goinv.Url_Allow_Test');
        }
    }

    protected function setDefault()
    {
        $this->postData = [
            'RespondType' => config('pay2goinv.RespondType'),
            'Version' => config('pay2goinv.Version_Allow'),
            'TimeStamp' => time(), // 需要為 time() 格式
            'InvoiceNo' => '',
            'MerchantOrderNo' => '',
            'ItemName' => '',
            'ItemCount' => 1, // 多商品以 | 隔開（string）
            'ItemUnit' => '',
            'ItemPrice' => 0,
            'ItemAmt' => 0,
            'TaxTypeForMixed' => '',
            'ItemTaxType' => '',
            'ItemTaxAmt' => 0,
            'TotalAmt' => 0,
            'BuyerEmail' => '',
            'Status' => config('pay2goinv.Status_Allow'),
        ];

        $this->taxRate = config('pay2goinv.TaxRate') * 0.01 + 1;
    }

    protected function multiDefault()
    {
        $this->postData['ItemPrice'] = [];
        $this->postData['ItemAmt'] = [];
        $this->postData['ItemTaxAmt'] = [];
    }

    protected function checkMaxTax($data)
    {
        if (isset($data['MaxTax'])) {
            if (is_array($this->postData['ItemTaxAmt'])) {
                $currentTotalTax = array_sum($this->postData['ItemTaxAmt']);

                if ($data['MaxTax'] < $currentTotalTax) {
                    $canUseTax = $data['MaxTax'] - $currentTotalTax + end($this->postData['ItemTaxAmt']);
                    $lastOne = array_key_last($this->postData['ItemTaxAmt']);

                    $this->postData['ItemAmt'][$lastOne] += ($this->postData['ItemTaxAmt'][$lastOne] - $canUseTax);
                    $this->postData['ItemTaxAmt'][$lastOne] = $canUseTax;
                }
            } else {
                if ($data['MaxTax'] < $this->postData['ItemTaxAmt']) {
                    $this->postData['ItemAmt'] += ($this->postData['ItemTaxAmt'] - $data['MaxTax']);
                    $this->postData['ItemTaxAmt'] = $data['MaxTax'];
                }
            }
        }
    }

    public function checkMultiItem()
    {
        parent::checkMultiItem();
        if (is_array($this->postData['ItemTaxAmt'])) {
            $this->postData['ItemTaxAmt'] = implode('|', $this->postData['ItemTaxAmt']);
        }
    }
}
