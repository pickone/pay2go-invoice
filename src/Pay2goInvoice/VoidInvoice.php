<?php

namespace Treerful\Invoice\Pay2goInvoice;

class VoidInvoice extends Pay2GoInvoice
{
    /*
     * 作廢發票的 class
     */

    public function setData($data)
    {
        $this->setDataByFields($data);
    }

    protected function setUrl()
    {
        if (!$this->debugMode) {
            $this->pay2goUrl = config('pay2goinv.Url_Void');
        } else {
            $this->pay2goUrl = config('pay2goinv.Url_Void_Test');
        }
    }

    protected function setDefault()
    {
        $this->postData = [
            'RespondType' => config('pay2goinv.RespondType'),
            'Version' => config('pay2goinv.Version_Void'),
            'TimeStamp' => time(), // 需要為 time() 格式
            'InvoiceNumber' => '',
            'InvalidReason' => '',
        ];
    }
}
