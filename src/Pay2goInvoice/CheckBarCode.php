<?php

namespace Treerful\Invoice\Pay2goInvoice;

class CheckBarCode extends Pay2GoInvoice
{
    /*
     * 查詢手機條碼是否存在於財政部
     */

    public function setData($barCode)
    {
        $this->postData['CellphoneBarcode'] = $barCode;
    }

    public function send()
    {
        $encryptData = $this->encrypt($this->postData);
        $checkValue = strtoupper(hash('sha256', "HashKey={$this->hashKey}&{$encryptData}&HashIV={$this->hashIv}"));

        $postDataArray = [
            'MerchantID_' => $this->merchantId,
            'Version' => config('pay2goinv.Version_CheckCode'),
            'RespondType' => config('pay2goinv.RespondType'),
            'PostData_' => $encryptData,
            'CheckValue' => $checkValue,
        ];

        $postDataQuery = http_build_query($postDataArray);

        return $this->sendPay2Go($postDataQuery);
    }

    protected function setUrl()
    {
        if (!$this->debugMode) {
            $this->pay2goUrl = config('pay2goinv.Url_Check_BarCode');
        } else {
            $this->pay2goUrl = config('pay2goinv.Url_Check_BarCode_Test');
        }
    }

    protected function setDefault()
    {
        $this->postData = [
            'TimeStamp' => time(), // 需要為 time() 格式
            'CellphoneBarcode' => '',
        ];
    }
}
