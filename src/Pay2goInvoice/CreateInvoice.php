<?php

namespace Treerful\Invoice\Pay2goInvoice;

class CreateInvoice extends Pay2GoInvoice
{
    /*
     * 開立發票的 class
     */
    protected $taxRate;

    public function setData($data): array
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }

        $this->setDataByFields($data);

        $this->setPostData($data);

        $this->checkMultiItem($data);

        return $this->postData;
    }

    private function setPostData($data)
    {
        if ($data['BuyerType'] == 2) {
            // B2B，有統編
            $this->postData['BuyerUBN'] = (string)$data['BuyerUBN'];
            $this->postData['Category'] = 'B2B';

            $this->postData['CarrierType'] = '';
            $this->postData['PrintFlag'] = 'Y'; // B2B 必填 Y

            if (is_array($data['ItemPrice'])) {
                // 多品項，個別價格給予含稅價
                $this->postData['ItemAmt'] = [];
                foreach ($data['ItemPrice'] as $key => $item) {
                    $this->postData['ItemAmt'][$key] = $item * $data['ItemCount'][$key];
                }
            } else {
                // 未稅價
                $this->postData['ItemPrice'] = round($data['ItemPrice'] / $this->taxRate);
                $this->postData['ItemAmt'] = round($data['ItemPrice'] * $data['ItemCount'] / $this->taxRate);
            }
        } else {
            // B2C，買受人個人
            $this->postData['BuyerUBN'] = '';
            $this->postData['Category'] = 'B2C';

            // 載具類別有提供時，LoveCode必為空值
            if ($data['BuyerType'] == 0) {
                $this->postData['CarrierType'] = (string)$data['CarrierType'];

                // 載具編號
                switch ($this->postData['CarrierType']) {
                    case '0': // 手機條碼
                    case '1': // 自然人
                        $this->postData['CarrierNum'] = rawurlencode($data['CarrierNum']);
                        break;

                    case '2': // 智付寶載具
                        $this->postData['CarrierNum'] = rawurlencode($data['BuyerEmail']);
                        break;

                    default:
                        $this->postData['CarrierNum'] = '';
                        // 載具類別、愛心碼皆為空時，索取發票必 Y
                        $this->postData['PrintFlag'] = 'Y';
                        break;
                }
            } elseif ($data['BuyerType'] == 1) {
                // 捐贈
                $this->postData['LoveCode'] = $data['LoveCode'];
                $this->postData['CarrierType'] = '';
            }

            // 含稅價
            if (is_array($data['ItemPrice'])) {
                $this->postData['ItemAmt'] = [];
                foreach ($data['ItemPrice'] as $key => $item) {
                    $this->postData['ItemAmt'][$key] = $item * $data['ItemCount'][$key];
                }
            } else {
                $this->postData['ItemPrice'] = $data['ItemPrice'];
                $this->postData['ItemAmt'] = $data['ItemPrice'] * $data['ItemCount'];
            }
        }

        // 零稅率、免稅的稅率 = 0
        if ($this->postData['TaxType'] == '2' || $this->postData['TaxType'] == '3') {
            $this->postData['TaxRate'] = 0;
        }

        // 課稅別為混合應稅
        if ($this->postData['TaxType'] == '9') {
            // 銷售額總計
            $amt = $this->postData['AmtSales'] + $this->postData['AmtZero'] + $this->postData['AmtFree'];
        } else {
            if (is_array($data['ItemPrice'])) {
                $total = 0;
                foreach ($data['ItemPrice'] as $key => $item) {
                    $total += $item * $data['ItemCount'][$key];
                }
                $amt = round($total / 1.05);
            } else {
                $amt = round($data['ItemPrice'] * $data['ItemCount'] / $this->taxRate);
            }
        }

        // 共同欄位
        $this->postData['Amt'] = $amt;
        $this->postData['TaxAmt'] = is_array($data['ItemPrice']) ? ($total - $amt) : ($data['ItemPrice'] * $data['ItemCount'] - $amt);
        $this->postData['TotalAmt'] = is_array($data['ItemPrice']) ? $total : $data['ItemPrice'] * $data['ItemCount'];
    }

    protected function setUrl()
    {
        if (!$this->debugMode) {
            $this->pay2goUrl = config('pay2goinv.Url_Create');
        } else {
            $this->pay2goUrl = config('pay2goinv.Url_Create_Test');
        }
    }

    protected function setDefault()
    {
        $this->postData = [
            'RespondType' => config('pay2goinv.RespondType'),
            'Version' => config('pay2goinv.Version_Create'),
            'TimeStamp' => time(), // 需要為 time() 格式
            'TransNum' => '',
            'MerchantOrderNo' => '',
            'BuyerName' => '',
            'BuyerUBN' => '',
            'BuyerAddress' => '',
            'BuyerEmail' => '',
            'Category' => config('pay2goinv.Category'),
            'TaxType' => config('pay2goinv.TaxType'),
            'TaxRate' => config('pay2goinv.TaxRate'),
            'CustomeClearance' => '',
            'Amt' => 0, // 未稅額
            'AmtSales' => 0,
            'AmtZero' => 0,
            'AmtFree' => 0,
            'TaxAmt' => 0,
            'TotalAmt' => 0,
            'CarrierType' => '',
            'CarrierNum' => '',
            'LoveCode' => '',
            'PrintFlag' => 'N',
            'ItemName' => '',
            'ItemCount' => 1,
            'ItemUnit' => '個',
            'ItemPrice' => 0,
            'ItemAmt' => 0,
            'ItemTaxType' => '',
            'Status' => config('pay2goinv.Status_Create'),
            'CreateStatusTime' => '',
            'Comment' => config('pay2goinv.Comment'),
        ];

        $this->taxRate = $this->postData['TaxRate'] * 0.01 + 1;
    }
}
