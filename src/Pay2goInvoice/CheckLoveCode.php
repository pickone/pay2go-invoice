<?php

namespace Treerful\Invoice\Pay2goInvoice;

class CheckLoveCode extends Pay2GoInvoice
{
    /*
     * 查詢捐贈碼是否存在於財政部
     */

    public function setData($loveCode)
    {
        $this->postData['LoveCode'] = $loveCode;
    }

    public function send()
    {
        $encryptData = $this->encrypt($this->postData);
        $checkValue = strtoupper(hash('sha256', "HashKey={$this->hashKey}&{$encryptData}&HashIV={$this->hashIv}"));

        $postDataArray = [
            'MerchantID_' => $this->merchantId,
            'Version' => config('pay2goinv.Version_CheckCode'),
            'RespondType' => config('pay2goinv.RespondType'),
            'PostData_' => $encryptData,
            'CheckValue' => $checkValue,
        ];

        $postDataQuery = http_build_query($postDataArray);

        return $this->sendPay2Go($postDataQuery);
    }

    protected function setUrl()
    {
        if (!$this->debugMode) {
            $this->pay2goUrl = config('pay2goinv.Url_Check_LoveCode');
        } else {
            $this->pay2goUrl = config('pay2goinv.Url_Check_LoveCode_Test');
        }
    }

    protected function setDefault()
    {
        $this->postData = [
            'TimeStamp' => time(), // 需要為 time() 格式
            'LoveCode' => '',
        ];
    }
}
