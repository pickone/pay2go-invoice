<?php

namespace Treerful\Invoice\Pay2goInvoice;

abstract class Pay2GoInvoice
{
    /*
     * 官方 API 文件：
     *     https://inv.pay2go.com/Invoice_index/download
     */

    protected $pay2goUrl; // 串接網址

    protected $merchantId;
    protected $hashKey;
    protected $hashIv;

    protected $debugMode = false;
    protected $postData;
    protected $rawResult;

    public function __construct()
    {
        $this->merchantId = config('pay2goinv.MerchantID');
        $this->hashKey = config('pay2goinv.HashKey');
        $this->hashIv = config('pay2goinv.HashIV');

        // default settings
        $this->debugMode = config('pay2goinv.Debug');
        $this->responseType = config('pay2goinv.RespondType');
        $this->postData = [];
        $this->setDefault();
        $this->setUrl();
    }

    // 設定postData參數, 因為每個方法要設定的資料不一樣，交給子類別實作
    abstract public function setData($data);

    // 根據傳入data逐一設定postData欄位
    public function setDataByFields(array $data)
    {
        foreach ($data as $key => $value) {
            $this->postData[$key] = $value;
        }
    }

    // 取得欲傳送資料
    public function getData(): array
    {
        return $this->postData;
    }

    // 加密資料，作業送出
    public function send()
    {
        $postData_ = $this->encrypt($this->postData);

        $postDataArray = [
            'MerchantID_' => $this->merchantId,
            'PostData_' => $postData_,
        ];

        $postDataQuery = http_build_query($postDataArray);

        return $this->sendPay2Go($postDataQuery);
    }

    // 加密函式
    protected function encrypt($data): string
    {
        /*
         * 使用 openssl_encrypt
         */
        $dataStr = http_build_query($data);
        return trim(
            bin2hex(
                openssl_encrypt(
                    $this->addpadding($dataStr),
                    'aes-256-cbc',
                    $this->hashKey,
                    OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING,
                    $this->hashIv
                )
            )
        );
    }

    // 加 padding
    protected function addpadding($string, $blocksize = 32): string
    {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);

        return $string;
    }

    // 資料送至 Pay2Go 串接網址，確認回傳資料內容
    protected function sendPay2Go($postDataQuery)
    {
        $result = $this->curlWork($this->pay2goUrl, $postDataQuery);
        $this->rawResult = $result;

        // $response['Status'], $response['Message'] ...
        return $this->checkResponse($result);
    }

    // curl post
    protected function curlWork($url = '', $parameter = ''): array
    {
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'Google Bot',
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POST => '1',
            CURLOPT_POSTFIELDS => $parameter,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_error = curl_errno($ch);
        curl_close($ch);

        return array(
            'url' => $url,
            'sent_parameter' => $parameter,
            'http_status' => $retcode,
            'curl_error_no' => $curl_error,
            'web_info' => $result,
        );
    }

    protected function checkResponse($result)
    {
        switch ($this->responseType) {
            case 'JSON':
                $response = json_decode($result['web_info'], true);
                if (!empty($response['Result'])) {
                    // 確認 回傳資料是否為 Json 格式，如果否則代表需要解密
                    if (is_string($response['Result'])) {
                        if (!$this->isJson($response['Result'])) {
                            $response['Result'] = $this->decrypt($response['Result']);
                        }
                        $resultData = json_decode($response['Result'], true);
                    }
                    foreach ($resultData as $key => $value) {
                        $response[$key] = $value;
                    }
                }
                break;
            case 'String':
                parse_str($result['web_info'], $response);
                break;
            default:
                $response = $result['web_info'];
                break;
        }

        return $response;
    }

    protected function isJson($string): bool
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    // 解密函式
    public function decrypt($data): bool|string
    {
        $dataDecrypt = $this->removePadding(
            openssl_decrypt(
                hex2bin(trim($data)),
                'aes-256-cbc',
                $this->hashKey,
                OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING,
                $this->hashIv
            )
        );

        parse_str($dataDecrypt, $output);
        return json_encode($output);
    }

    // 移除 padding
    protected function removePadding($string): string
    {
        $result = '';
        for ($i = 0; $i < strlen($string); $i++) {
            if (ord($string[$i]) > 32) {
                $result .= $string[$i];
            }
        }
        return $result;
    }

    // 回傳原始結果
    public function getRawResult()
    {
        return $this->rawResult;
    }

    public function checkMultiItem()
    {
        if (is_array($this->postData['ItemName'])) {
            $this->postData['ItemName'] = implode('|', $this->postData['ItemName']);
            $this->postData['ItemCount'] = implode('|', $this->postData['ItemCount']);
            $this->postData['ItemUnit'] = implode('|', $this->postData['ItemUnit']);
            $this->postData['ItemPrice'] = implode('|', $this->postData['ItemPrice']);
            $this->postData['ItemAmt'] = implode('|', $this->postData['ItemAmt']);
        }
    }
}
