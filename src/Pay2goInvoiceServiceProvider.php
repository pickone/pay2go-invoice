<?php

namespace Treerful\Invoice;

use Illuminate\Support\ServiceProvider;

class Pay2goInvoiceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/pay2goinv.php' => config_path('pay2goinv.php'),
            __DIR__ . '/../config/mof-invoice.php' => config_path('mof-invoice.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
