<?php

namespace Treerful\Invoice\MofInvoice;

class CheckBarCode extends MofInvoice
{
    protected $action;
    protected $barCode;
    protected $TxID;

    public function setData($code = ''): static
    {
        $this->action = 'bcv';
        $this->barCode = $code;
        $this->TxID = 'Bar' . time();

        return $this;
    }
}
