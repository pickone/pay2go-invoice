<?php

namespace Treerful\Invoice\MofInvoice;

class Invoice
{
    protected $invoice = null;

    public function checkBarCode($code)
    {
        $this->invoice = new CheckBarCode();
        $this->invoice->setData($code);

        return $this->invoice->send();
    }

    public function checkLoveCode($code)
    {
        $this->invoice = new CheckLoveCode();
        $this->invoice->setData($code);

        return $this->invoice->send();
    }
}
