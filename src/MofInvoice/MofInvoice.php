<?php

namespace Treerful\Invoice\MofInvoice;

abstract class MofInvoice
{
    protected $appId;
    protected $url;
    protected $version;

    public function __construct()
    {
        $this->appId = config('mof-invoice.appId');
        $this->url = config('mof-invoice.url');
        $this->version = config('mof-invoice.version');
    }

    abstract public function setData($data = '');

    public function send()
    {
        $result = $this->runCurl(http_build_query($this));

        return json_decode($result);
    }

    public function runCurl($data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        if ($result === false) {
            $error = curl_error($ch);
        }
        curl_close($ch);

        if (isset($error)) {
            return false;
        }

        return $result;
    }
}
