<?php

namespace Treerful\Invoice\MofInvoice;

class CheckLoveCode extends MofInvoice
{
    protected $action;
    protected $pCode;
    protected $TxID;

    public function setData($code = ''): static
    {
        $this->action = 'preserveCodeCheck';
        $this->pCode = $code;
        $this->TxID = 'LoveCode' . time();

        return $this;
    }
}
