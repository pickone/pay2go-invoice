<?php

return [
    'version' => '1.0',

    'url' => 'https://www-vc.einvoice.nat.gov.tw/BIZAPIVAN/biz',

    'appId' => env('MOF_INVOICE_APPID'),
];
