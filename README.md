# pay2go-invoice （智付寶 Pay2go 電子發票）
一個包含 Laravel 與 台灣電子發票處理的套件。  
這個套件包含三個部分：  
**pay2go 電子發票(或稱 ezPay電子發票)的串接。**  
透做串接，可利用 pay2go 進行發票的「開立」、「折讓」、「作廢」、「查詢」並將結果以 Array 傳回。  
註：原先還有規劃查詢『捐贈碼』以及『手機載具條碼』的功能，但 pay2go 端功能尚未完整，因此功能待開發。  

**財政部電子發票營業人應用 API 的部分功能串接**  
可以進行『捐贈碼』以及『手機載具條碼』的檢查，判斷該條碼是否存在於電子發票整合服務平台。
註：此功能須向財政部提出申請

**經濟部商工行政資料開放平臺 API 的部分功能串接**  
可以利用統編查詢公司名稱、商號。
註：此功能須向商工行政資料開放平臺提出申請

A package about handle Laravel and Taiwan e-invoice  
This pakcage contain three part：  
**pay2go(or ezPay)**  
with this, we can create, void, allowance, search invoice on pay2go. The result will return by array.

**Ministry of Finance(Mof) E-invoice**  
Use to check carrier and love code exists or not.
Note: This feature requires application

**Government Communication and Information System(GCIS) Open Data**
Use to find the company name by tax ID number.
Note: This feature requires application

## Getting Started
- [開立發票](#create)
- [折讓發票](#allow)
- [作廢發票](#void)
- [查詢發票](#search)
- [查詢手機條碼](#check-bar-code)
- [查詢捐贈碼](#check-love-code)
- [查詢公司名稱](#get-company-name)
- [查詢商號名稱](#get-business-name)

### Install
1. Install from composer:
```
composer require treerful/pay2go-invoice
```

2. Copy the config file (2 methods):

(a) In `config/app.php`,
```
    'providers' => [
        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        ...
        ...
        Treerful\Invoice\Pay2goInvoiceServiceProvider::class,
```

After adding the ServiceProvider, publish the config file.
```
php artisan vendor:publish
```

(b) Because the proivder is only for publishing the config, you can also copy it manually without adding the provider.
```
cp vendor/treerful/pay2go-einvoice/config/pay2goinv.php config/pay2goinv.php
cp vendor/treerful/pay2go-einvoice/config/mof-invoice.php config/mof-invoice.php
```

*Remember that the filename `pay2goinv.php` and `mof-invoice.php` should not be changed!*

### Pay2go 引用、初始化類別：
```
use Treerful\Invoice\Pay2goInvoice\Invoice;
```

<a id="create"></a>

### 開立發票：
開立發票共有三種模式，分別為 B2C, 捐贈, B2B。
本 Package 使用欄位 *BuyerType* （非智付寶官方提供欄位）做區分，此欄位為必填。

若需要開立多品項發票，則 `ItemName, 'ItemPrice', 'ItemUnit', 'ItemCount` 需為陣列形式
且個別 `ItemPrice` 為含稅價
在計算時會使用總額直接計算稅額

1. B2C (BuyerType = 0)
```
$data = [
    'BuyerType' => '0', // B2C
    'MerchantOrderNo' => <自訂商品編號>,
    'BuyerName' => <買受人姓名>,
    'CarrierType' => <載具類別>, // 0:手機條碼, 1:自然人憑證條碼載具 2:智付寶載具
    'CarrierNum' => <載具編號>,
    'ItemName' => <商品名稱> | [商品1, 商品2],
    'ItemCount' => <商品數量> | [商品1數量, 商品2數量],
    'ItemPrice' => <商品單價> | [商品1單價, 商品2單價],
    'ItemUnit' => <商品單位> | [商品1單位, 商品2單位],
];
```

2. 捐贈 (BuyerType = 1)
```
// 捐贈
$data = [
    'BuyerType' => '1',
    'MerchantOrderNo' => <自訂商品編號>,
    'BuyerName' => <買受人姓名>,
    'LoveCode' => <愛心碼>,
    'ItemName' => <商品名稱> | [商品1, 商品2],
    'ItemCount' => <商品數量> | [商品1數量, 商品2數量],
    'ItemPrice' => <商品單價> | [商品1單價, 商品2單價],
    'ItemUnit' => <商品單位> | [商品1單位, 商品2單位],
];
```

3. B2B（BuyerType = 2）
```
// B2B
$data = [
    'BuyerType' => '2', // B2B
    'MerchantOrderNo' => <自訂商品編號>,
    'BuyerName' => <買受人姓名>,
    'BuyerUBN' => <買受人統一編號>,
    'ItemName' => <商品名稱> | [商品1, 商品2],
    'ItemCount' => <商品數量> | [商品1數量, 商品2數量],
    'ItemPrice' => <商品單價> | [商品1單價, 商品2單價],
    'ItemUnit' => <商品單位> | [商品1單位, 商品2單位],
];
```

呼叫函式
```
$invoice = new Invoice();
$result = $invoice->create(data);
```

*[補充]*
- 除了上述欄位外，可參考官方API提供其他需要的欄位，例如：*BuyerAddress*(買受人地址), *BuyerEmail*(買受人信箱), *Comment*(發票備註)。
- 商品數量預設為 1。
- 商品單位若不設置，預設為「個」。

<a id="allow"></a>

### 折讓發票：
```
$data = [
    'InvoiceNo' => <發票號碼>,
    'MerchantOrderNo' => <自訂商品編號>,
    'ItemName' => <商品名稱> | [商品1, 商品2],
    'ItemCount' => <商品數量> | [商品1數量, 商品2數量],
    'ItemPrice' => <商品單價> | [商品1單價, 商品2單價],
    'ItemUnit' => <商品單位> | [商品1單位, 商品2單位],
    'BuyerEmail' => <買受人信箱>, // 可不填，若填寫則智付寶將寄信通知買受人。
    'BuyerType' => 2 // 2 for B2B，國稅局規定 B2B, B2C 稅額計算方式不同。
    'MaxTax' => <商品最大折稅額> (!不必須),
];

$invoice = new Invoice();
$result = $invoice->allow($data);
```
*[補充]*
- 商品數量預設為 1。
- 商品單位若不設置，預設為「個」。
- 商品稅額檢查，若為多品項且需要調整時，會直接使用最後一項進行調整

<a id="void"></a>

### 作廢發票：
```
$data = [
    'InvoiceNumber' => <發票號碼>,
    'InvalidReason' => <作廢理由>,
];
$invoice = new Invoice();
$result = $invoice->void($data);
```

<a id="search"></a>

### 查詢發票：
查詢發票的回傳結果有兩種顯示方式，一為直接回傳資料，二為以Post Form的方式導向智付寶發票頁面，可在 `config/pay2goinv.php` 中設定 `DisplayFlag` 的值。
```
// 直接回傳資料
// 'DisplayFlag' => '',

// 導向智付寶發票頁面
// 'DisplayFlag' => '1',
```

查詢發票的查詢方式亦有兩種，一為發票金額＆隨機碼，二為自訂商品編號與總金額，可在 `config/pay2goinv.php` 中設定 `SearchType` 的值。
```
// 發票金額&隨機碼
// 'SearchType' => '0'
// 商品編號&總金額
// 'SearchType' => '1'
```

根據上述兩種方式：
```
// When SearchType is 0
```
$data = [
    'InvoiceNumber' => <發票號碼>,
    'RandomNum' => <隨機碼>,
];
```
// When SearchType is 1
$data = [
    'MerchantOrderNo' => <自訂商品編號>,
    'TotalAmt' => <商品總金額>,
];

$invoice = new Invoice();
$result = $invoice->search($data);
```

### Notes
- 以上的 `$data` 可接受 Object or Array 兩種型態。
- 除了上述欄位外，其他欄位請參考 [智付寶API](https://inv.pay2go.com/Invoice_index/download)，並自行在`$data`中新增參數即可

## Other Usage:
- 當呼叫過四個函式其中一個後，除了原本回傳的結果外，亦可使用 `getPostData()` 取得類別中實際送出的資料。
```
// Example: (This will get nothing.)
$invoice = new Invoice();
$result = $invoice->getPostData();

// Example: (Cont. This will get the original data you posted to Pay2go.)
$invoice = new Invoice();
$invoice->create($data);
$postData = $invoice->getPostData();
```

- 當呼叫過四個函式其中一個後，除了原本回傳的結果外，亦可使用 `getRawResult()` 取得API呼叫結果後的原始資料。
```
// Example: (This will get nothing.)
$invoice = new Invoice();
$result = $invoice->getRawResult();

// Example: (This will get the raw data you got from Pay2go.)
$invoice = new Invoice();
$invoice->create($data);
$rawResult = $invoice->getRawResult();
```

### 財政部電子發票營業人應用 API 引用、初始化類別：
```
use Treerful\Invoice\MofInvoice\Invoice;
```

<a id="check-bar-code"></a>

### 查詢手機條碼
```
$invoice = new Invoice();
$response = $invoice->checkBarCode($barCode);

$response['IsExist'] == 'Y' or 'N'
// 'Y' 表示此條碼存在於財政婦，'N' 表示此條碼查無資料
```

<a id="check-love-code"></a>

### 查詢捐贈碼
```
$invoice = new Invoice();
$response = $invoice->checkLoveCode($loveCode);

$response['IsExist'] == 'Y' or 'N'
// 'Y' 表示此捐贈碼存在於財政婦，'N' 表示此捐贈碼查無資料
```

### 濟部商工行政資料開放平臺 API 引用、初始化類別：
```
use Treerful\Invoice\GCIS\Invoice;
```

<a id="get-company-name"></a>

### 查詢公司名稱
```
$invoice = new Invoice();
$response = $invoice->getCompanyName($companyNum);

// response example: string type of: [{"Business_Accounting_NO":"20828393","Company_Name":"宏碁股份有限公司"}]
```

<a id="get-business-name"></a>

### 查詢商號名稱
```
$invoice = new Invoice();
$response = $invoice->getBusinessName($companyNum);

// response example: string type of: [{"Business_Name":"宏碁股份有限公司"}]
```

## Authors

* **Yifan Wu** - *Initial work* - [Github](https://github.com/yfancc20)
* **Treerful** - [Github](https://github.com/treerful)

## Official Reference
[Pay2go E-Invoice API](https://inv.pay2go.com/Invoice_index/download)
[Pay2go 手機條碼與捐贈碼驗證技術串接手冊](https://inv.ezpay.com.tw/dw_files/info_api/BDV_1_0_0.pdf)
[財政部電子發票營業人應用](https://www.einvoice.nat.gov.tw/home/DownLoad?fileName=1468833776540_0.pdf)
[經濟部商工行政資料開放平臺開發指引](https://data.gcis.nat.gov.tw/od/rule)
